<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Untitled Document</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.button.min.css" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="jQueryAssets/jquery-1.11.1.min.js"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.button.min.js"></script>
</head>

<body>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      <a class="navbar-brand" href="#">E-LIBRARY</a></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="defaultNavbar1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="Untitled-3.php">Home<span class="sr-only">(current)</span></a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
          <ul class="dropdown-menu">
           <li><a href="list.php">List Book</a></li>
            <li><a href="managebook.php">Manage Book</a></li>
            <li><a href="managebookcritic.php">Manage Book Critic</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
               <li>
		<a href="untitled-3.php">Log Out </a>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.js"></script>
  <!-- /.container-fluid -->
<br>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		  <center><h1> Update The Critic </h1></center>

 <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
    <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <br />
    <form class="form-horizontal form-label-left" method="POST" action="">

      <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Name Of Criticus<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo !empty($name)?$name:'';?>" />
      </div>
      </div>

	    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Address<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo !empty($address)?$address:'';?>" />
      </div>
      </div> 
	  
	  <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">ISBN Book<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="isbn" name="isbn" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo !empty($isbn)?$isbn:'';?>" />
      </div>
      </div>  
	  
      <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Book Title<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
	       <select>
    <option name="genre" value="management system book">The Book Of Management System</option>
   <option name="genre" value="book of technical engineering support">The Book Of Technical Engineering Support</option>
   <option name="genre" value="book of tutorial being a developer">The Book Of Tutorial Being A Web Developer</option>
   <option name="genre" value="The Book Of Elly">The Book Of Elly </option>
   <option name="genre" value="The Book Of Government Technical">The Book Of Government Technical</option>
        </select>
      </div>
      </div>
	  
	  <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Genre<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
     <select>
   <option name="genre" value="IT">IT</option>
   <option name="genre" value="Manajemen">Manajemen</option>
   <option name="genre" value="Developer">Developer</option>
   <option name="genre" value="Government">Government</option>
   <option name="genre" value="Governance">Governance</option>
   <option name="genre" value="Medicine">Medicine</option>
   <option name="genre" value="Life Style">Life Style</option>
   <option name="genre" value="Ingredient recive">Ingredient recive</option>
   <option name="genre" value="Game">Game</option>
   <option name="genre" value="Autovaganza">Autovaganza</option>
   </select>
      </div>
      </div>
	  
      <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Book Tipe<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
	   <select>
    <option name="genre" value="History">History</option>
   <option name="genre" value="Knowledge">Knowledge</option>
   <option name="genre" value="Religious">Religious</option>
   <option name="genre" value="Creativity & Education">Creativity & Education</option>
   <option name="genre" value="Child Drama History">Child Drama History</option>
   <option name="genre" value="Automotive Magazine">Automotive Magazine</option>
   <option name="genre" value="Cooking Magazine">Cooking Magazine</option>
   <option name="genre" value="Game Magazine">Game Magazine</option>
   <option name="genre" value="Urgent Report Book">Urgent Report Book</option>
</select>
      </div>
      </div>
	  
	  	<div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Year Published<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="Created" name="Created" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo !empty($Created)?$Created:'';?>" />
      </div>
      </div> 

      <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment & Critic <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
      <textarea name=textbox1 rows=10 cols=88></textarea>
      </div>
      </div>
	   </div>
      <div class="ln_solid"></div>
      <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="cancel" class="btn btn-primary">Cancel<a href="list.php">'</button>
        <input type="submit" id="submit" name="submit" class="btn btn-success" value="SAVE" />
      </div>
    </form>
 </table>	
	</tbody>
                    </table>	
				</div>
			</div>
		</div>
<div class="media-object-default">
  <div class="media">
    <div class="media-left"><a href="#"></a></div>
    <div class="media-body">
<br><br>

</div>
 </div>
</div>
<ul class="pager">
  <ul class="pager">
</body>