<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Untitled Document</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.button.min.css" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="jQueryAssets/jquery-1.11.1.min.js"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.button.min.js"></script>
</head>

<body>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      <a class="navbar-brand" href="#">E-LIBRARY</a></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="defaultNavbar1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="Untitled-3.php">Home<span class="sr-only">(current)</span></a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="list.php">List Book</a></li>
     <li><a href="managebook.php">Manage Book</a></li>
            <li><a href="managebookcritic.php">Manage Book Critic</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
	   <li><a href="#">Profile</a></li>
        		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sign<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="Login.html">Login</a></li>
            <li><a href="Register.php">Register</a></li>
            <li><a href="Help.php">Help</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="disclaimer.php">Legal Disclaimer</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>
<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4"></div>
  <div class="col-md-4"></div>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.js"></script>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
<br>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
<br>
<center>
<div class "table">
<table> 
<br>

<div class="row">
	<div class="col-lg-8 col-sm-12">
		<h1>elibrary</h1>
		
		<p class="mb-3">Openl Libraries are provides for access to our eBooks, eAudio, eMagazines, online databases and digital book reference heritage, catalogues and storytime videos for achieve the object target(improving human skill,knowledge base,behavior,creativity of educational component,soft skill and hard skill for ready do all heavy task when do a job on industi.</p>	                    
		<div>
			<div class="wclmini px-3 py-4 text-center">
				<a href="http://www.wcl.govt.nz/blog/index.php/2016/08/11/new-wcl-mini-app/">
					<img src="/downloads/images/Nikau-grey.png" class="" alt="WCL Mini">
				</a>
				<h2 class="pt-3">
					<a href="http://www.wcl.govt.nz/blog/index.php/2016/08/11/new-wcl-mini-app/">WCL Mini</a>
				</h2>
				<p>It's a library in your pocket! WCL Mini is the Wellington City Libraries app for mobile devices. Search the catalogue, place reserves, renew items, check your card, and use the virtual library card, all from the app.</p>
			</div>
		</div>
		
		<div class="mb-3 pb-3 mt-4">
			<h2>Get started</h2>
			<p>Need some help getting up and running quickly with any of our eLibrary services? Look no further!</p>
			
			<a href="/downloads/getting-started-with-our-ebooks-eAudiobooks.html" class="btn btn-primary btn-block">Getting started guides</a>
		</div>
		
		<hr>
		
		<div class="mb-3 pb-3">
			<h2>Technical Support</h2>
			<p>If you are having problems using our catalogues, online databases, eMedia or any other part of our website, please fill out our technical support form and we will provide support where we can.</p>
			
			<a href="/help/#technical" class="btn btn-primary btn-block">Submit tech help request</a>
		</div>
		
		<hr>
		
		<div class="mb-3 pb-3">
			<h2>Library apps</h2>
			<p>In addition to our own WCL library app, several of the online resources available via our website offer a mobile app to support enhanced browsing, listening and reading on a mobile device, either table or smartphone.</p>
			
			<a href="/about/services/apps.html" class="btn btn-primary btn-block">Browse library apps</a>
		</div>
		
		<hr>
		
		<div class="mb-3 pb-3 border-bottom-1">
			<h2>Online stories</h2>
			<p>View and listen to stories from New Zealand and around the world on your computer in the comfort of your own home!</p>
			
			<a href="/kids/downloads" class="btn btn-primary btn-block">Online stories</a>
		</div>
		
	</div>
</div>