<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Register Open Library</title>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.button.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style-1.css"> <!-- pemanggilan file css untuk style pada file index-1.html -->
    <meta name="viewport" content="width=device-width , initial-scale=1">
  </head>
  <body>

    <div id="login"><!-- membuat sebuah div id dengan tujuan sebagai background utama  -->
      <div class="center"><!-- div dengan class center bertujuan untuk membuat posisi tag form yang akan dibuat akan menjadi rata tengah -->

          <h2>Register Your Account To Open Library</h2> <!-- membuat judul pembuka -->

          <form class="fl" action="" method="post">
            nama:<input class="itpw" type="text" name="nama" placeholder="nama"><br>
            alamat:<input class="itpw" type="text" name="alamat" placeholder="alamat">
			telepon:<input class="itpw" type="text" name="telepon" placeholder="telepon">
			Gender: <input type="radio" name="gender" value="L" checked>Laki-Laki &nbsp;&nbsp;||&nbsp;&nbsp;<input type="radio" name="gender" value="P">Perempuan<br/>
			<br>
			username:<input class="itpw" type="text" name="username" placeholder="username">
			password:<input class="itpw" type="text" name="password" placeholder="password">
			<br>
           <center><a href="success.php" class="btn btn-primary" role="button">Register</a>&nbsp; &nbsp; <a href="untitled-3.php" class="btn btn-danger" role="button">Cancel</a></center>
          </form>

      </div>
    </div>

  </body>
</html>