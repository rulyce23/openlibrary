<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Untitled Document</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.button.min.css" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="jQueryAssets/jquery-1.11.1.min.js"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.button.min.js"></script>
</head>

<body>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      <a class="navbar-brand" href="#">E-LIBRARY</a></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="defaultNavbar1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="Untitled-2.php">Home<span class="sr-only">(current)</span></a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="list2.php">List Book</a></li>
            <li><a href="managebook2.php">Manage Book</a></li>
            <li><a href="managebookcritic2.php">Manage Book Critic</a></li>
            <li><a href="manageaccount.php">Manage Account</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="profile.php">Profile</a></li>
        <li>
		<a href="untitled-3.php">Log Out </a>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.js"></script>
<br>
<div id="carousel1" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel1" data-slide-to="1"></li>
    <li data-target="#carousel1" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item active"><img src="images/Carousel_Placeholder.jpg" height="350" width="700" alt="First slide image" class="center-block">
      <div class="carousel-caption">
        <h3>Welcome To Open Library</h3>
        <p>Version 1.0.0</p>
      </div>
    </div>
    <div class="item"><img src="images/library.jpg" height="350" width="700" alt="Second slide image" class="center-block">
      <div class="carousel-caption">
        <h3>Want Find Some Great Book ?</h3>
        <p>In This Site Has Some List Of Book & Search Button </p>
      </div>
    </div>
    <div class="item"><img src="images/Carousel2.jpg" height="350" width="700" alt="Third slide image" class="center-block">
      <div class="carousel-caption">
        <h3>Are you Like The Book ?</h3>
        <p>You Can Download & Critic Some Book You Are being Liked</p>
      </div>
    </div>
  </div>
  <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="right carousel-control" href="#carousel1" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span></a></div>
<div class="row">
<div class="col-md-4">
  <div class="thumbnail"><img src="images/information.jpg" alt="Thumbnail Image 1">
    <div class="caption">
         <h3>The Book Of Management Information System</h3>
          <p>Describe about Management Information System</p>
      <p><a href="criticpage1.php" class="btn btn-primary" role="button">Add Some Critic</a><a href="listdetailbeforelogin.php" class="btn btn-default" role="button">View Detail</a><a href="data/optic.pptx" class="btn btn-success" role="button">Download</a></p>
        </div>
      </div>
    </div>
<div class="row">
  <div class="col-md-4">
      <div class="thumbnail"><img src="images/engineer.jpg" alt="Thumbnail Image 1">
        <div class="caption">
          <h3>The Book Of Technical Engineering Support</h3>
          <p>Describe about Technical Engine Support</p>
          <p><a href="criticpage2.php" class="btn btn-primary" role="button">Add Some Critic</a><a href="viewdetail2.php" class="btn btn-default" role="button">View Detail</a><a href="data/Engineering-in-society.pdf" class="btn btn-success" role="button">Download</a></p>
        </div>
      </div>
    </div>
<div class="row">
  <div class="col-md-4">
      <div class="thumbnail"><img src="images/web.jpg" alt="Thumbnail Image 1">
        <div class="caption">
          <h3>The Book Of Tutorial Being A Web Developer</h3>
          <p>Describe about Step by step Become a Web Developer</p>
          <p><a href="criticpage3.php" class="btn btn-primary" role="button">Add Some Critic</a><a href="viewdetail3.php" class="btn btn-default" role="button">View Detail</a><a href="data/webmaster.pdf" class="btn btn-success" role="button">Download</a></p>
        </div>
        </div>
    </div>
  </div>
</div>
<div class="media-object-default">
  <div class="media">
    <div class="media-left"><a href="#"></a></div>
    <div class="media-body">
	
</div>
<ul class="pager">
  <li class="previous disabled"><a href="#"><span aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &larr;</span> Older</a></li>
  <li class="next"><a href="#">Newer <span aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;</span></a></li>
</ul>
<script type="text/javascript">
$(function() {
	$( "#Button1" ).button(); 
});
</script>
</body>
</html>