<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Untitled Document</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.button.min.css" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="jQueryAssets/jquery-1.11.1.min.js"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.button.min.js"></script>
</head>

<body>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      <a class="navbar-brand" href="#">E-LIBRARY</a></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="defaultNavbar1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="Untitled-3.php">Home<span class="sr-only">(current)</span></a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="list.php">List Book</a></li>
     <li><a href="managebook.php">Manage Book</a></li>
            <li><a href="managebookcritic.php">Manage Book Critic</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
	   <li><a href="#">Profile</a></li>
        		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sign<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="Login.html">Login</a></li>
            <li><a href="Register.php">Register</a></li>
            <li><a href="Help.php">Help</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="disclaimer.php">Legal Disclaimer</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>
<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4"></div>
  <div class="col-md-4"></div>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.js"></script>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
<br>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
<br>
<center>
<div class "table">
<table> 
<br>

<h1> Disclaimer for OpenLibrary</h1>
<p> If you require any more information or have any questions about our site's disclaimer, please feel free to contact us by email at <a href="&#109;&#97;&#105;&#108;&#116;&#111;:&#114;&#117;&#108;&#121;&#114;&#105;&#122;&#107;&#121;&#112;&#101;&#114;&#100;&#97;&#110;&#97;&#64;&#121;&#97;&#104;&#111;&#111;&#46;&#99;&#111;&#46;&#105;&#100;">089681889629</a><h2>Disclaimers for library Team RuRiAl:</h2>
<p>
All the information on this website is published in good faith and for general information purpose only. library.lpkia.ac.id does not make any warranties about the completeness, reliability and accuracy of this information. Any action you take upon the information you find on this website (library.lpkia.ac.id), is strictly at your own risk. library.lpkia.ac.id will not be liable for any losses and/or damages in connection with the use of our website.
</p>
<p>
From our website, you can visit other websites by following hyperlinks to such external sites. While we strive to provide only quality links to useful and ethical websites, we have no control over the content and nature of these sites. These links to other websites do not imply a recommendation for all the content found on these sites. Site owners and content may change without notice and may occur before we have the opportunity to remove a link which may have gone 'bad'.
</p>
<p>
Please be also aware that when you leave our website, other sites may have different privacy policies and terms which are beyond our control. Please be sure to check the Privacy Policies of these sites as well as their "Terms of Service" before engaging in any business or uploading any information.
</p>

<h3>Consent</h3>
<p>
By using our website, you hereby consent to our disclaimer and agree to its terms.
</p><h3>Update</h3>This site disclaimer was last updated on: Monday, July 3rd, 2017<br /><em> · Should we update, amend or make any changes to this document, those changes will be prominently posted here.</em><br /><br /><hr />
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.js"></script>
<div class="media-object-default">
  <div class="media">
    <div class="media-left"><a href="#"></a></div>
    <div class="media-body">