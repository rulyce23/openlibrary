<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Untitled Document</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.button.min.css" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="jQueryAssets/jquery-1.11.1.min.js"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.button.min.js"></script>
</head>

<body>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      <a class="navbar-brand" href="#">E-LIBRARY</a></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="defaultNavbar1">
      <ul class="nav navbar-nav">
      <li class="active"><a href="Untitled-2.php">Home<span class="sr-only">(current)</span></a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="list2.php">List Book</a></li>
            <li><a href="managebook2.php">Manage Book</a></li>
            <li><a href="managebookcritic2.php">Manage Book Critic</a></li>
            <li><a href="manageaccount.php">Manage Account</a></li>
            <li><a href="managejournal.php">Manage Journal</a></li>
          </ul>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
	   <li><a href="#">Profile</a></li>
              <li>
		<a href="untitled-3.php">Log Out </a>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
<br>
<div class="container-fluid">
  <div class="container"></div>
</div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
<br>
<center>
<div class "table">
<table> 
<br>
 <tr><td>Book Name:</td><td><input type="text" value="" name="name" /><td colspan="2" align="center"><center>&nbsp;<button type="submit" class="btn btn-default">Search</button></center></td></tr> 
 </table>
 <br>
   <div class="right_col" role="main"> 
  <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_title">
                    <h2>List Of Books</h2>
                    <div class="clearfix"></div>
                  </div>
                    <table class="table media-left">
                      <thead>
                        <tr>
                          <th>Book TItle</th>
                          <th>Description</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>The Book Of Management Information System</td>
                          <td>Management Information System</td>
                          <td>
							<a href="view.php" class="btn btn-success" role="button">View Description</a></td>
                        	 <td>
							<a href="#" class="btn btn-primary" role="button">View Detail</a></td>
						  </td>
						</tr>
                        <tr>
                          <td>The Book Of Technical Engineering Support</td>
                          <td>Technical Engine Support</td>
                          <td>
							<a href="view2.php" class="btn btn-success" role="button">View Description</a></td>
                        	 <td>
							<a href="#" class="btn btn-primary" role="button">View Detail</a></td>
						  </td>
                        </tr>
                        <tr>
                          <td>The Book Of Tutorial Being A Web Developer</td>
                          <td>Step by step Become a Web Developer</td>
                          <td>
							<a href="view3.php" class="btn btn-success" role="button">View Description</a></td>
                        	  <td>
							<a href="#" class="btn btn-primary" role="button">View Detail</a></td>
						  </td>
						  <tr>
                          <td>The Book Of Elly</td>
                          <td>History Telling Of Elly</td>
                          <td>
							<a href="view4.php" class="btn btn-success" role="button">View Description</a></td>
                        	  <td>
							<a href="#" class="btn btn-primary" role="button">View Detail</a></td>
						  </td>
						  <tr>
                          <td>The Book Of Goverment Technical</td>
                          <td>Management Principle technical of Goverment</td>
                          <td>
							<a href="view5.php" class="btn btn-success" role="button">View Description</a></td>
                        	  <td>
							<a href="#" class="btn btn-primary" role="button">View Detail</a></td>
						  </td>
						  </td>
                        </tr>
                      </tbody>
                    </table>	
				</div>
			</div>
		</div>
		<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.js"></script>
<div class="media-object-default">
  <div class="media">
    <div class="media-left"><a href="#"></a></div>
    <div class="media-body">
<br><br>

</div>
 </div>
</div>
<ul class="pager">
  <ul class="pager">
  <li class="previous disabled"><a href="#"><span aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &larr;</span> Older</a></li>
  <li class="next"><a href="#">Newer <span aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;</span></a></li>
</ul>
</body>